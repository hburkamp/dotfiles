###########
# History #
###########

export HISTFILE=~/.histfile
export HISTSIZE=1000000   # the number of items for the internal history list
export SAVEHIST=1000000   # maximum number of items for the history file

setopt HIST_IGNORE_ALL_DUPS  # do not put duplicated command into history list
setopt HIST_SAVE_NO_DUPS  # do not save duplicated command
setopt HIST_REDUCE_BLANKS  # remove unnecessary blanks
setopt INC_APPEND_HISTORY_TIME  # append command to history file immediately after execution
setopt EXTENDED_HISTORY  # record command start time

###########
# Options #
###########

setopt AUTO_PUSHD           # Push the current directory visited on the stack.
setopt PUSHD_IGNORE_DUPS    # Do not store duplicates in the stack.
setopt PUSHD_SILENT         # Do not print the directory stack after pushd or popd.

##########
# Prompt #
##########

fpath=($ZDOTDIR $fpath)
autoload -Uz prompt.zsh; prompt.zsh

###########
# Aliases #
###########

alias d='dirs -v'
for index ({1..9}) alias "$index"="cd +${index}"; unset index
# Create alias for nvim.appimage installation, in case repo version (debian, ubuntu, ...) is to old
if [[ -a $HOME/nvim.appimage ]]; then
    alias nvim='$HOME/nvim.appimage'
fi

alias ll='ls -lah --color=auto'

############
# Bindkeys #
############

bindkey -s '^f' 'tmux-sessionizer.sh^M'

########
# tmux #
########

# Fix tmux startup on WSL2
if [ -f "/proc/sys/fs/binfmt_misc/WSLInterop" ]; then
    export TMUX_TMPDIR='/tmp'
    # Add System32 to PATH for clip.exe to work with neovim in WSL2
    export PATH="$PATH:/mnt/c/Windows/System32"
    export PATH="$PATH:/mnt/c/Windows/System32/WindowsPowerShell/v1.0"
fi


################
# Config files #
################

for file in $ZDOTDIR/*.zsh(.); source $file

source $ZDOTDIR/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $ZDOTDIR/zsh-autosuggestions/zsh-autosuggestions.zsh

##################
# Path additions #
##################

PATH=$PATH:$HOME/.cargo/bin/

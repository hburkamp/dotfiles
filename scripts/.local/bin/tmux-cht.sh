#!/usr/bin/env bash

# Script taken from https://github.com/ThePrimeagen/.dotfiles

langs=~/dotfiles/tmux/.config/tmux/tmux-cht-languages
cmds=~/dotfiles/tmux/.config/tmux/tmux-cht-command

selected=$(cat "$langs" "$cmds" | fzf)
if [[ -z $selected ]]; then
    exit 0
fi

read -p "Enter Query: " query

if grep -qs "$selected" "$langs"; then
    query=$(echo "$query" | tr ' ' '+')
    tmux neww bash -c "curl -s cht.sh/$selected/$query | less -r"
else
    tmux neww bash -c "curl -s cht.sh/$selected~$query | less -r"
fi

#     ______         
#    / / __/_________
#   / / /_/ ___/ ___/
#  / / __/ /  / /__  
# /_/_/ /_/   \___/  

#################
# Shell options #
#################

set shell sh
set shellopts '-eu'
set ifs "\n"

#################
# Basic options #
#################

set scrolloff 10
set cursorpreviewfmt "\033[7;2m"
# set preview true
set hidden true
set drawbox true
set icons true
set ignorecase true

##################
# Custm commands #
##################

# define a custom 'open' command
# This command is called when current file is not a directory. You may want to
# use either file extensions and/or mime types here. Below uses an editor for
# text files and a file opener for the rest.
cmd open &{{
    case $(file --mime-type -Lb $f) in
        text/*) lf -remote "send $id \$$EDITOR \$fx";;
        *) for f in $fx; do $OPENER $f > /dev/null 2> /dev/null & done;;
    esac
}}

# define a custom 'delete' command
cmd delete ${{
    set -f
    printf "$fx\n"
    printf "delete?[y/n]"
    read ans
    [ "$ans" = "y" ] && rm -rf $fx
}}

# extract the current file with the right command
# (xkcd link: https://xkcd.com/1168/)
cmd extract ${{
    set -f
    case $f in
        *.tar.bz|*.tar.bz2|*.tbz|*.tbz2) tar xjvf $f;;
        *.tar.gz|*.tgz) tar xzvf $f;;
        *.tar.xz|*.txz) tar xJvf $f;;
        *.zip) unzip $f;;
        *.rar) unrar x $f;;
        *.7z) 7z x $f;;
    esac
}}

# compress current file or selected files with tar and gunzip
cmd tar ${{
    set -f
    mkdir $1
    cp -r $fx $1
    tar czf $1.tar.gz $1
    rm -rf $1
}}

# compress current file or selected files with zip
cmd zip ${{
    set -f
    mkdir $1
    cp -r $fx $1
    zip -r $1.zip $1
    rm -rf $1
}}

###########
# Keymaps #
###########

# Directory mappings
map gh cd ~
map gw $lf -remote "send $id cd $OLDPWD"
map g. cd ~/dotfiles
map gd cd ~/Documents
map gD cd ~/Downloads
map gn cd ~/Nextcloud
map gW. cd /mnt/c/Users/hendrikburkamp/Desktop/
map gWd cd /mnt/c/Users/hendrikburkamp/Documents/
map gWD cd /mnt/c/Users/hendrikburkamp/Downloads/
map gWs cd /mnt/c/Users/hendrikburkamp/Desktop/Skripte/Python/Stunden/

# use enter for shell commands
map <enter> shell

# show the result of execution of previous commands
map ` !true

# execute current file (must be executable)
map x $$f
map X !$f

# dedicated keys for file opener actions
map o &mimeopen $f
map O $mimeopen --ask $f


# mkdir and touch commands
map a :push %mkdir<space>
map A :push %touch<space>

# define a custom 'rename' command without prompt for overwrite
# cmd rename %[ -e $1 ] && printf "file exists" || mv $f $1
# map r push :rename<space>

# make sure trash folder exists
# %mkdir -p ~/.trash

# move current file or selected files to trash folder
# (also see 'man mv' for backup/overwrite options)
cmd trash %set -f; mv $fx ~/.trash


# use '<delete>' key for either 'trash' or 'delete' command
# map <delete> trash
map <delete> delete


#!/usr/bin/env python

###########
# Imports #
###########
import glob
import re

from Xlib import display as xdisplay
from Xlib.ext.randr import Connected as RR_Connected

########################
# Function Definitions #
########################


def is_closed_lid(output: str):
    """Determine if laptop lid is closed Taken from autorandr: https://github.c
    om/phillipberndt/autorandr/blob/4f010c576/autorandr.py#L99-L108."""
    if not re.match(r"(eDP(-?[0-9]\+)*|LVDS(-?[0-9]\+)*)", output):
        return False
    lids = glob.glob("/proc/acpi/button/lid/*/state")
    if len(lids) == 1:
        state_file = lids[0]
        with open(state_file) as f:
            content = f.read()
            return "close" in content
    return False


def get_num_monitors() -> int:
    """Get the number of monitors activated on the computer Taken from jrwrigh:

    https://github.com/jrwrigh/dotfiles2.0/blob/T14/.config/qtile/functions.py.
    """
    num_monitors = 0
    try:
        display = xdisplay.Display()
        screen = display.screen()
        resources = screen.root.xrandr_get_screen_resources()._data

        for output in resources["outputs"]:
            monitor = display.xrandr_get_output_info(
                output, resources["config_timestamp"]
            )._data
            if monitor["connection"] == RR_Connected and not is_closed_lid(
                monitor["name"]
            ):
                num_monitors += 1
    except Exception:
        return 1  # always return at least 1 monitor
    else:
        return num_monitors


def export_keys(keys, path):
    pass

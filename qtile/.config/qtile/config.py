#!/usr/bin/env python

"""
TODO: Write docstring
"""

###########
# Imports #
###########

import os
import subprocess
from typing import List  # noqa: F401

from color_themes import Style, gruvbox
from custom import get_num_monitors
from libqtile import bar, hook, layout, qtile, widget
from libqtile.config import Click, Drag, Group, Key, KeyChord, Match, Screen
from libqtile.lazy import lazy
from libqtile.log_utils import logger
from libqtile.utils import guess_terminal

###########
# Globals #
###########

mod = "mod4"
terminal = guess_terminal()

##########
# Styles #
##########

styles = {
    "gruvbox": Style(
        palette=gruvbox,
        foreground="white",
        background="black",
        primary="green",
        secondary="orange",
        tetriary="blue",
        emacs_theme="doom-gruvbox",
    ),
}
current_style = styles["gruvbox"]

########
# Keys #
########

keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod], "n", lazy.next_screen(), desc="Move to the next screen"),
    Key(
        [mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"
    ),
    Key(
        [mod, "shift"],
        "l",
        lazy.layout.shuffle_right(),
        desc="Move window to the right",
    ),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key(
        [mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"
    ),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod, "control"], "n", lazy.layout.normalize(),
        desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key(
        [mod],
        "r",
        lazy.spawn("rofi -show run"),
        desc="Spawn a command using a prompt widget",
    ),
    # Sound
    Key([], "XF86AudioMute", lazy.spawn("amixer -q set Master toggle")),
    Key(
        [],
        "XF86AudioLowerVolume",
        lazy.spawn(f"amixer -q set Master 5%- unmute"),
        desc="Lower audio volume",
    ),
    Key(
        [],
        "XF86AudioRaiseVolume",
        lazy.spawn(f"amixer -q set Master 5%+ unmute"),
        desc="Raise audio volume",
    ),
    # backlight controls
    Key([], "XF86MonBrightnessUp", lazy.spawn("light -A 10")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("light -U 10")),
    # KeyChords
    KeyChord([mod], "s", [Key([], "r", lazy.restart()),
             Key([], "s", lazy.shutdown())]),
]

##########
# Groups #
##########

groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend(
        [
            # mod1 + letter of group = switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # mod1 + shift + letter of group = switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(
                    i.name),
            ),
        ]
    )

###########
# Layouts #
###########

layout_theme = {
    "border_width": 1,
    "border_focus": current_style["secondary"],
}

layouts = [
    layout.Columns(**layout_theme),
    layout.Max(**layout_theme),
]

widget_defaults = dict(
    font="Hack Nerd Font",
    fontsize=12,
    padding=3,
    foreground=current_style["foreground"],
)
extension_defaults = widget_defaults.copy()

###########
# Screens #
###########

num_screens = get_num_monitors()
logger.warning(f"Number of monitors: {num_screens}")

screens = [
    Screen(
        bottom=bar.Bar(
            [
                widget.Image(
                    filename="~/.config/qtile/icons/python-white.png",
                    scale="False",
                ),
                widget.GroupBox(
                    active=current_style["primary"],
                    block_highlight_text_color=current_style["foreground"],
                    this_current_screen_border=current_style["primary"],
                    highlight_method="block",
                    rounded=False,
                ),
                widget.CurrentLayoutIcon(),
                widget.Prompt(),
                widget.WindowName(),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.TextBox(text="CPU:"),
                widget.CPUGraph(
                    fill_color=current_style["background"],
                    border_color=current_style["foreground"],
                    graph_color=current_style["primary"],
                ),
                widget.TextBox(text="RAM:"),
                widget.MemoryGraph(
                    fill_color=current_style["background"],
                    border_color=current_style["foreground"],
                    graph_color=current_style["secondary"],
                ),
                widget.TextBox(text="TMP:"),
                widget.ThermalSensor(),
                widget.TextBox(text="BAT:"),
                widget.Battery(
                    format="{char} {percent:2.0%} ({hour:d}:{min:02d}h)",
                    empty_char="🗌",
                    full_char="█",
                    unknown_char="⍰",
                    charge_char="↑",
                    discharge_char="↓",
                ),
                widget.Systray(),
                widget.Clock(
                    format="%Y-%m-%d %a %H:%M", foreground=current_style["secondary"]
                ),
            ],
            24,
            background=current_style["background"],
        ),
    ),
]

for screen in range(1, num_screens):
    screens.append(
        Screen(
            bottom=bar.Bar(
                [
                    widget.Image(
                        filename="~/.config/qtile/icons/python-white.png",
                        scale="False",
                    ),
                    widget.GroupBox(
                        active=current_style["primary"],
                        block_highlight_text_color=current_style["foreground"],
                        this_current_screen_border=current_style["primary"],
                        highlight_method="block",
                        rounded=False,
                    ),
                    widget.CurrentLayoutIcon(),
                    widget.WindowName(),
                    widget.Chord(
                        chords_colors={
                            "launch": ("#ff0000", "#ffffff"),
                        },
                        name_transform=lambda name: name.upper(),
                    ),
                    widget.TextBox(text="CPU:"),
                    widget.CPUGraph(
                        fill_color=current_style["background"],
                        border_color=current_style["foreground"],
                        graph_color=current_style["primary"],
                    ),
                    widget.TextBox(text="RAM:"),
                    widget.MemoryGraph(
                        fill_color=current_style["background"],
                        border_color=current_style["foreground"],
                        graph_color=current_style["secondary"],
                    ),
                    widget.TextBox(text="TMP:"),
                    widget.ThermalSensor(),
                    widget.TextBox(text="BAT:"),
                    widget.Battery(
                        format="{char} {percent:2.0%} ({hour:d}:{min:02d}h)",
                        empty_char="🗌",
                        full_char="█",
                        unknown_char="⍰",
                        charge_char="↑",
                        discharge_char="↓",
                    ),
                    widget.Clock(
                        format="%Y-%m-%d %a %H:%M",
                        foreground=current_style["secondary"],
                    ),
                ],
                24,
                background=current_style["background"],
            )
        )
    )

# Drag floating layouts.
mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
    ),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

##########
# Custom #
##########

# Exporting shortcuts
# TODO: create function for export


# Autostart applications
@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser("~")
    subprocess.call([home + "/.config/qtile/autostart.sh"])


# Reload config when number of screens changes
@hook.subscribe.screen_change
def restart_on_randr(_):
    lazy.restart()

#!/usr/bin/env bash

lxsession &
picom &
feh --bg-max --randomize ~/.wallpaper/* &
/usr/bin/emacs --daemon &
volumeicon &
nm-applet &
nextcloud-client --background &
redshift &
dunst &
unclutter &
play-with-mpv &

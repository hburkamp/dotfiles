# Dotfiles

[[_TOC_]]

## What's inside

This repository contains my personal dotfiles.
You will find configuration files for the following programs:

| Category           | Name                                                                                                | Config file                                                                                                                                                           |
|--------------------|-----------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Editors            | [Neovim](https://neovim.io/)<br>[Ideavim](https://github.com/JetBrains/ideavim)                     | [init.vim](https://gitlab.com/hburkamp/dotfiles/-/tree/main/nvim/.config/nvim)<br>[.ideavimrc](https://gitlab.com/hburkamp/dotfiles/-/tree/main/ideavim)              |
| Shells             | [Zsh](https://www.zsh.org/)                                                                         | [.zshrc](https://gitlab.com/hburkamp/dotfiles/-/tree/main/zsh/.config/zsh)                                                                                            |
| Window managers    | [Qtile](http://www.qtile.org/)<br>[i3](https://i3wm.org/)<br>[Awesome](https://awesomewm.org/)      | [config.py](https://gitlab.com/hburkamp/dotfiles/-/tree/main/qtile/.config/qtile)<br>[config](https://gitlab.com/hburkamp/dotfiles/-/tree/main/i3/.config/i3)<br>     |
| Terminal emulators | [Alacritty](https://alacritty.org/)                                                                 | [alacritty.yml](https://gitlab.com/hburkamp/dotfiles/-/tree/main/alacritty/.config/alacritty)                                                                         |
| Misc.              | User config<br>[Ranger](https://ranger.github.io/)<br>[Zathura](https://pwmt.org/projects/zathura/) | [.profile](https://gitlab.com/hburkamp/dotfiles/-/tree/main/user_config)<br><br>[zathurarc](https://gitlab.com/hburkamp/dotfiles/-/tree/main/zathura/.config/zathura) |



## Using this repository

You can use the configuration files in this repository easily via [GNU stow](https://www.gnu.org/software/stow/):

1. Clone this repository

	```bash
	git clone https://gitlab.com/hburkamp/dotfiles.git
	```

2. Deploy the config files via stow and makefile

	```bash
	cd dotfiles
	make
	```

local wk = require 'which-key'
local telescope = require 'telescope.builtin'

-- Leader in normal mode
wk.register({
  -- Spelling
  p = {
    name = 'S[p]elllang',
    g = { ':set spelllang=de<CR>', 'Spelllang [G]erman' },
    e = { ':set spelllang=en<CR>', 'Spelllang [E]nglish' },
  },
  -- Undotree
  u = { vim.cmd.UndotreeToggle, 'Toggle [U]ndotree' },
  -- Additional telescope bindings
  s = {
    v = { telescope.git_files, 'Search [V]CS' },
  },
  -- Netrw
  n = { '<cmd>e .<CR>', '[E]xplore [.]cwd' },
  -- Git mappings
  g = {
    name = '+[G]it',
    g = { ':Git<Space>', '[G]it prompt' },
    s = { telescope.git_status, '[S]tatus' },
    a = {
      name = '+[A]dd',
      a = { ':Git<Space>add<Space>.<CR>', '[A]dd .' },
      p = { ':Git<Space>add<Space>-p<CR>', '[P]atch' },
    },
    c = {
      name = '+[C]ommit',
      c = { ':Git<Space>commit<CR>', '[C]ommit' },
      l = { telescope.git_commits, '[L]og all commits' },
      b = { telescope.git_bcommits, '[L]og commits for current buffer' },
    },
    t = { telescope.git_stash, 'Show S[t]ash' },
    m = { ':Git<Space>merge<CR>', '[M]erge' },
    d = { ':Gvdiffsplit<CR>', '[D]iff' },
    f = {
      name = '+[F]ile',
      m = { ':GMove<CR>', '[M]ove' },
      d = { ':GDelete<CR>', '[D]elete' },
    },
    l = {
      ":Git<Space>log<Space>--graph<Space>--pretty=format:'%Cred%h%Creset<Space>-%C(yellow)%d%Creset<Space>%s<Space>%Cgreen(%cr)%Creset'<Space>--abbrev-commit<Space>--date=relative<CR>",
      '[L]og',
    },
    r = {
      name = '+[R]emote',
      s = { ':Git<Space>push<CR>', 'Pu[s]h' },
      l = { ':Git<Space>pull<CR>', 'Pu[l]l' },
      f = { ':Git<Space>fetch<CR>', '[F]etch' },
    },
    b = {
      name = '+[B]ranch',
      b = { telescope.git_branches, 'List all [B]ranches' },
      n = { ':Git<Space>checkout<Space>-b<Space>', 'Checkout [N]ew branch' },
      c = { ':Git<Space>checkout<Space>', '[C]heckout existing branch' },
    },
    w = {
      name = '+[W]orktree',
      a = { ':Git<Space>worktree<Space>add<Space>', '[A]dd new worktree' },
      b = { ':Git<Space>worktree<Space>add<Space>-b<Space>', 'Add new worktree based on a new [B]ranch' },
      r = { ':Git<Space>worktree<Space>remove<Space>', '[R]emove existing worktree' },
      l = { ':Git<Space>worktree<Space>list<CR>', '[L]ist existing worktrees' },
    },
  },
  -- Markdown mappings
  m = {
    name = '+[M]arkdown',
    p = { '<CMD>MarkdownPreview<CR>', 'Start markdown [P]review' },
    s = { '<CMD>MarkdownPreviewStop<CR>', '[S]top markdown preview' },
    t = { '<CMD>MarkdownPreviewToggle<CR>', '[T]oggle markdown preview' },
  },
  -- Cellular Automaton
  a = {
    name = '+Cellular [A]utomaton',
    r = { '<cmd>CellularAutomaton make_it_rain<CR>', 'Make it [R]ain' },
    g = { '<cmd>CellularAutomaton game_of_life<CR>', '[G]ame of life' },
  },
}, { mode = 'n', prefix = '<leader>' })

-- normal mode
wk.register({
  -- Navigate between panels
  ['<C-Left>'] = { '<C-w>h', 'Move window [L]eft' },
  ['<C-Down>'] = { '<C-w>j', 'Move window [D]own' },
  ['<C-Up>'] = { '<C-w>k', 'Move window [U]p' },
  ['<C-Right>'] = { '<C-w>l', 'Move window [R]ight' },
  -- Save and quit
  ['<C-s>'] = { '<cmd>:w<CR>', '[S]ave buffer' },
  ['<C-w>'] = { '<cmd>:bd<CR>', '[D]elete buffer' },
  ['<C-q>'] = { '<cmd>:q<CR>', '[Q]uit buffer' },
  -- Better mappings for search
  ['n'] = { 'nzz', '[N]ext search result and center line (zz)' },
  ['N'] = { 'Nzz', 'Previous (N) search result and center line (zz)' },
}, { mode = 'n' })

-- visual mode
wk.register({
  -- format markdown tables
  ['<C-t>'] = { "!column -t -s '|' -o '|'<CR>", 'Format markdown [T]ables' },
}, { mode = 'v' })

-- terminal mode
wk.register({
  -- Navigate between panels
  ['<C-Left>'] = { '<C-\\><C-N><C-w>h', 'Move window [L]eft' },
  ['<C-Down>'] = { '<C-\\><C-N><C-w>j', 'Move window [D]own' },
  ['<C-Up>'] = { '<C-\\><C-N><C-w>k', 'Move window [U]p' },
  ['<C-Right>'] = { '<C-\\><C-N><C-w>l', 'Move window [R]ight' },
}, { mode = 't' })

-- General settings
vim.fn.matchadd('errorMsg', [[\s\+$]]) -- Highlight trailing whitespace
vim.o.scrolloff = 20
vim.o.loaded_spellfile_plugin = 1
vim.opt.spelllang = 'en_us'
vim.opt.spell = true

-- Clipboard Settings
vim.o.clipboard = 'unnamedplus' -- Sync clipboard between OS and Neovim.
if vim.fn.has 'wsl' == 1 then -- Use system clipboard / WSL fix
  vim.g.clipboard = {
    name = 'WslClipboard',
    copy = {
      ['+'] = 'clip.exe',
      ['*'] = 'clip.exe',
    },
    paste = {
      ['+'] = 'powershell.exe -c [Console]::Out.Write($(Get-Clipboard -Raw).tostring().replace("`r", ""))',
      ['*'] = 'powershell.exe -c [Console]::Out.Write($(Get-Clipboard -Raw).tostring().replace("`r", ""))',
    },
    cache_enabled = 0,
  }
end

-- Settings for netrw
vim.g.netrw_keepdir = 0
vim.g.netrw_winsize = 25
vim.g.netrw_banner = 0
vim.g.netrw_liststyle = 3

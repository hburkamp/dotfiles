-- You can add your own plugins here or in other files in this directory!
--  I promise not to create any merge conflicts in this directory :)
--
-- See the kickstart.nvim README for more information
return {
  -- Git related plugins
  'tpope/vim-fugitive',
  'mbbill/undotree',
  {
    -- Markdown preview
    'iamcco/markdown-preview.nvim',
    cmd = { 'MarkdownPreviewToggle', 'MarkdownPreview', 'MarkdownPreviewStop' },
    ft = { 'markdown' },
    build = function()
      vim.fn['mkdp#util#install']()
    end,
  },

  {
    -- LaTeX support
    'lervag/vimtex',
    init = function()
      vim.g.vimtex_view_method = 'zathura'
    end,
  },

  {
    -- For when nothing else works
    'eandrju/cellular-automaton.nvim',
  },
}
